#!/usr/bin/env python3

import pfycat
import secret


# ###########################
# ########## tests ##########
# ###########################

def test_live_service(client):
    assert client.users("stabbot")["userid"] == "stabbot"
    assert client.gfycats("recentfalsecurlew")["gfyItem"]["gfyId"] == "recentfalsecurlew"


def test_live_upload(client):
    # print(client.upload(video_path, {"noMd5": True}))
    r = client.upload('tests/banana.gif')
    print(r)
    assert r["numFrames"] == 8.0
    assert r["height"] == 360


def test_live_auth(client):
    # this only works when there is a authenticated user
    assert client.me()["userid"]
    print(client.me())


# ###########################
# ###### configuration ######
# ###########################

pfycat.log_waiting = True
pfycat.log_requests = False
pfycat.log_responses = False

client_anonym = pfycat.Client()

client_authed = pfycat.Client(secret.client_id, secret.client_secret)
client_password = pfycat.Client(secret.client_id, secret.client_secret, secret.username, secret.password)

# ###########################
# ######## execution ########
# ###########################

print("testing no-auth")
test_live_service(client_anonym)
test_live_upload(client_anonym)

print("testing client_auth")
test_live_service(client_authed)
test_live_upload(client_authed)

print("testing user-auth")
test_live_auth(client_password)
test_live_service(client_password)
test_live_upload(client_password)

print("all tests done")